terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}


variable "zone_name" {
  default = "ru-central1-a"
}

provider "yandex" {
  zone      = "${var.zone_name}"
}


variable "instance-id" {
  default = "fd8fte6bebi857ortlja"
}

variable "subnet-id" {
  default = "e9bjfn8djh8k9npf3ko7"
}
variable "network-id" {
  default = "enp46jrfdj2k8gberj1s"

}

resource "yandex_compute_instance" "web-app" {
  name          = "web-app"
  platform_id   = "standard-v1"
  zone          = "${var.zone_name}"
  

  resources {
    cores  = 2
    memory = 1
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "${var.instance-id}" #ubuntu 20.04  put in vars
      size     = 15
    }
  }

network_interface {
    subnet_id = "${var.subnet-id}" # put in vars
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  scheduling_policy{
    preemptible = true
  }


}




resource "yandex_alb_target_group" "alb_target_group" {
  name      = "my-target-group"

  target {
    subnet_id = "${var.subnet-id}" # put in vars
    ip_address   = "${yandex_compute_instance.web-app.network_interface.0.ip_address}"
  }
}



resource "yandex_alb_backend_group" "test-backend-group" {
  name      = "my-backend-group"

  http_backend {
    name = "test-http-backend"
    weight = 1
    port = 80
    target_group_ids = ["${yandex_alb_target_group.alb_target_group.id}"]
  
    load_balancing_config {
      panic_threshold = 50
    }    
    healthcheck {
      timeout = "1s"
      interval = "1s"
      healthcheck_port = 80
      http_healthcheck {
        path  = "/"
      }
    }
    
  }
}


resource "yandex_alb_http_router" "tf-router" {
  name      = "my-http-router"

}

resource "yandex_alb_virtual_host" "ws-virtual-host" {
  name           = "ws-virtual-host"
  http_router_id = yandex_alb_http_router.tf-router.id
  route {
    name = "socketio"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.test-backend-group.id
        #timeout          = "3s"
        #upgrade_types    = ["websocket"]
      }
    }
  }
}


resource "yandex_alb_load_balancer" "test-balancer" {
  name        = "my-load-balancer"

  network_id  = "${var.network-id}" # put in vars

  allocation_policy {
    location {
      zone_id   = "${var.zone_name}"
      subnet_id = "${var.subnet-id}" # put in vars
    }
  }
  
  
  listener {
    name = "redirect-listener"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80 ]
    }    
    http {
      redirects {
        http_to_https = true
      }
    }
    }
   

listener {
    name = "my-listener2"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 443 ]
    }    
   tls{
    default_handler{
      
         http_handler  {
        http_router_id = yandex_alb_http_router.tf-router.id
            
            }

       
            
        certificate_ids = ["fpqbh98uakp8002irg37"]

        
      } 
    }
  } 

}


resource "template_file" "inventory" {
  template = file("${path.module}/inventory.tpl")
  
  vars = {
    user = "ubuntu"
    host = join("", [yandex_compute_instance.web-app.name, " ansible_host=", yandex_compute_instance.web-app.network_interface.0.nat_ip_address])
  }
}

resource "local_file" "save_inventory" {
  content  = template_file.inventory.rendered
  filename = "${path.module}/ansible/inventories/dev/hosts"
}
